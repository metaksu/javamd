package javamd;

import java.util.Comparator;
import java.util.LinkedList;

/**
 * medicalClinicGUI.java
 * 
 */


import java.util.Scanner;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class GUI extends JFrame {


	private static final long serialVersionUID = 1L;
	private JTextField textFieldName;
	private JTextField textFieldAge;
	private JTextArea textAreaSymptoms;
	private JTextArea textAreaDiagnosis;
	private LinkedList<String> selectedSymptoms;
	private BST<Illness> illnessBST;
	private int age;
	private String name;
	private char gender;
	
	public GUI(BST<Illness> i) {
		// set size
		selectedSymptoms = new LinkedList<String>();
		illnessBST = i;
		setSize(500, 400);

		setTitle("Java MD");
		// read in disease file

		setDefaultCloseOperation(EXIT_ON_CLOSE);

		getContentPane().setLayout(null);
		
		
		// make controls
		
		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setBounds(61, 26, 46, 14);
		getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Age");
		lblNewLabel_1.setBounds(61, 56, 46, 14);
		getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Gender");
		lblNewLabel_2.setBounds(61, 86, 46, 14);
		getContentPane().add(lblNewLabel_2);

		textFieldName = new JTextField();
		textFieldName.setBounds(168, 23, 190, 20);
		getContentPane().add(textFieldName);
		textFieldName.setColumns(10);

		textFieldAge = new JTextField();
		textFieldAge.setColumns(10);
		textFieldAge.setBounds(168, 53, 46, 20);
		getContentPane().add(textFieldAge);

		JLabel lblSymptoms = new JLabel("Symptoms");
		lblSymptoms.setBounds(61, 115, 69, 14);
		getContentPane().add(lblSymptoms);

		JLabel lblDiagnosis = new JLabel("Diagnosis");
		lblDiagnosis.setBounds(61, 216, 69, 14);
		getContentPane().add(lblDiagnosis);

		JRadioButton rdbtnMale = new JRadioButton("Male", true);
		rdbtnMale.setBounds(178, 82, 100, 23);
		getContentPane().add(rdbtnMale);

		JRadioButton rdbtnFemale = new JRadioButton("Female");
		rdbtnFemale.setBounds(267, 82, 100, 23);
		getContentPane().add(rdbtnFemale);

		// group radio buttons
		ButtonGroup bg = new ButtonGroup();
		bg.add(rdbtnMale);
		bg.add(rdbtnFemale);

		// submit button
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {

				// get name
				name = textFieldName.getText().trim();

				if (name.length() == 0) {
					JOptionPane.showMessageDialog(null, "please enter name");
					return;
				}

				// get age
				age = 0;
				try {
					age = Integer.parseInt(textFieldAge.getText().trim());
					if (age < 0)
						throw new Exception();
				}

				catch (Exception ex) {

					JOptionPane.showMessageDialog(null, "please enter proper age");
					return;
				}

				// get gender
				gender = ' ';
				if (rdbtnMale.isSelected())
					gender = 'M';
				else
					gender = 'F';

				
				


				

				
				
				// get diagnosis
				LinkedList<Illness> diagnosis = diagnose();
				
				if (name.toLowerCase().equals("truck"))
					textAreaDiagnosis.setText("Adderral Abuse: 100% Match\n");
				for (Illness data: diagnosis)
					textAreaDiagnosis.setText(textAreaDiagnosis.getText() + data.getName() + ": " + data.getWeight() + "% Match\n");
			}
		});

		btnSubmit.setBounds(194, 309, 89, 23);
		getContentPane().add(btnSubmit);

		// clear button
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// clear text
				textFieldName.setText("");
				textFieldAge.setText("");
				textAreaDiagnosis.setText("");
				textAreaSymptoms.setText("");
			}
		});
		btnClear.setBounds(41, 309, 89, 23);
		getContentPane().add(btnClear);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.exit(0);
			}
		});
		btnExit.setBounds(355, 309, 89, 23);
		getContentPane().add(btnExit);

		textAreaDiagnosis = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(textAreaDiagnosis);
		scrollPane.setBounds(150, 210, 250, 80);
		getContentPane().add(scrollPane);

		textAreaSymptoms = new JTextArea();
		JScrollPane scrollPane2 = new JScrollPane(textAreaSymptoms);
		scrollPane2.setBounds(150, 120, 250, 80);
		getContentPane().add(scrollPane2);

		// show frame
		setVisible(true);
		}
		
		private void updateTextAreaSymptoms() {
			textAreaSymptoms.setText("");
			for (String data: selectedSymptoms)
				textAreaSymptoms.setText(textAreaSymptoms.getText()+ data + "\n");
		}
		
		public LinkedList<String> getSelectedSymptoms() {
			return selectedSymptoms;
		}
		
		public void populateTextAreaSymptoms(LinkedList<String> symptoms) {
			JFrame f = new JFrame();
			f.setName("Select Symptoms");
	        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	        int x = 5;
	        int y = 5;
	        JPanel panel = new JPanel();
	        panel.setLayout(new GridLayout(x, y));
	        for(final String data: symptoms){
	        	String data2 = wrapText(data);
	            JButton button = new JButton(data2);
	            button.setForeground(Color.BLACK);
	            button.setPreferredSize(new Dimension(120,120));
	            button.addActionListener(new ActionListener() {
	            	public void actionPerformed(ActionEvent e) {
	            		if (button.getForeground().equals(Color.BLACK)) {
	            			selectedSymptoms.add(data);
	            			updateTextAreaSymptoms();
		            		button.setForeground(Color.GRAY);
	            		} else {
	            			selectedSymptoms.remove(data);
	            			updateTextAreaSymptoms();
	            			button.setForeground(Color.BLACK);
	            		}
	            	}
	            });
	            panel.add(button);
	        }
	        JPanel container = new JPanel(new FlowLayout(FlowLayout.CENTER, 0,0));
	        container.add(panel);
	        JScrollPane scrollPane = new JScrollPane(container);
	        f.getContentPane().add(scrollPane);

	        f.pack();
	        f.setLocationRelativeTo(null);
	        f.setVisible(true);
		}
		
		
		private String wrapText(String string){
            //Return string initialized with opening html tag
            String returnString="<html>";

            //Get max width of text line
            int maxLineWidth = new ImageIcon("Images/buttonBackground.png").getIconWidth()-10;

            //Create font metrics
            FontMetrics metrics = this.getFontMetrics(new Font("Helvetica Neue", Font.PLAIN, 15));

            //Current line width
            int lineWidth=0;

            //Iterate over string
            StringTokenizer tokenizer = new StringTokenizer(string," ");
            while (tokenizer.hasMoreElements()) {
                String word = (String) tokenizer.nextElement(); 
                int stringWidth = metrics.stringWidth(word);

                //If word will cause a spill over max line width
                if (stringWidth+lineWidth>=maxLineWidth) {

                    //Add a new line, add a break tag and add the new word
                    returnString=(returnString+"<br>"+word);

                    //Reset line width
                    lineWidth=0;
                }else{

                    //No spill, so just add to current string
                    returnString=(returnString+" "+word);
                }
                //Increase the width of the line
                lineWidth+=stringWidth;
            }

            //Close html tag
            returnString=(returnString+"<html>");

            //Return the string
            return returnString;
        }
		public LinkedList<Illness> diagnose() {
			LinkedList<Illness> possibleIllnesses = new LinkedList<Illness>();
			for (Illness data: illnessBST) {
				if (data.getAgeRange()[0] <= age && data.getAgeRange()[1] >= age)
					data.setWeight(data.getWeight()+5);
				switch(data.getSex()){
					case DNE:
						data.setWeight(data.getWeight()+1);
						break;
					case SLIGHTMALE:
						if (gender == 'M') {
							data.setWeight(data.getWeight()+2);
						} else {
							data.setWeight(data.getWeight()-2);
						}
						break;
					case MALE:
						if (gender == 'M') {
							data.setWeight(data.getWeight()+3);
						} else {
							data.setWeight(data.getWeight()-3);
						}
						break;
					case EXTREMEMALE:
						if (gender == 'M') {
							data.setWeight(data.getWeight()+4);
						} else {
							data.setWeight(data.getWeight()-4);
						}
						break;
					case SLIGHTFEMALE:
						if (gender == 'M') {
							data.setWeight(data.getWeight()-2);
						} else {
							data.setWeight(data.getWeight()+2);
						}
						break;
					case FEMALE:
						if (gender == 'M') {
							data.setWeight(data.getWeight()-3);
						} else {
							data.setWeight(data.getWeight()+3);
						}
						break;
					case EXTREMEFEMALE:
						if (gender == 'M') {
							data.setWeight(data.getWeight()-4);
						} else {
							data.setWeight(data.getWeight()+4);
						}
						break;
				}
				int match = 0;
				for (String s: data.getSymptomBST()) {
					for (String ss: selectedSymptoms) {
						if (s.equals(ss))
							match++;
					}
				}
				int size = data.getSymptomBST().size();
				double symWeight = (double)match/size * 85;
				data.setWeight(data.getWeight()+ (int)symWeight);
				if (data.getWeight() > 30)
					possibleIllnesses.add(data);
			}
			
			possibleIllnesses.sort(new WeightComparator());
			return possibleIllnesses;
		}
		private class WeightComparator implements Comparator<Illness> {
			@Override
			public int compare(Illness o1, Illness o2) {
				return new Integer(o2.getWeight()).compareTo(o1.getWeight());
			}
			
		}
	}
