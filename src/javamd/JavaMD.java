package javamd;
import java.io.*;
import java.util.Comparator;
import java.util.LinkedList;
/**
 * The instantiated class that handles visual and functional aspects of JavaMD
 * @author metinaksu
 *
 */
public class JavaMD {
	private BST<Illness> illnessBST;
	private BufferedReader br;
	private LinkedList<String> symptomsBank;
	
	/**
	 * Constructor for the class
	 */
	public JavaMD() {
		illnessBST = new BST<Illness>();
		symptomsBank = new LinkedList<String>();
		try {
			br = new BufferedReader(new FileReader("illnessDoc.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		populateIllness();
		populateSymptomsBank();
		GUI gui = new GUI(illnessBST);
		gui.populateTextAreaSymptoms(symptomsBank);
	}
	/**
	 * Populates the illnessBST object that holds all illnesses the program can search for
	 * Works by reading in from an organized text file of illnesses and their respective data and adding the illnesses to the illnessBST
	 */
	public void populateIllness(){
		String currentLine = null;
		String rName = null;
		String rCategory = null;
		int[] rAgeRange = new int[2];
		SexBias rSB = null;
		BST<String> rSymptoms = new BST<String>();
		try {
			currentLine = br.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while (currentLine != null) {
			try {
				rName = currentLine;
				rCategory = br.readLine();
				rAgeRange[0] = Integer.parseInt(br.readLine());
				rAgeRange[1] = Integer.parseInt(br.readLine());
				rSB = SexBias.valueOf(br.readLine());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				currentLine = br.readLine();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			rSymptoms = new BST<String>();
			while (!currentLine.equals("*")) {
				try {
					rSymptoms.insert(currentLine);
					currentLine = br.readLine();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			illnessBST.insert(new Illness(rName, rAgeRange, rSB, rCategory, rSymptoms));
			try {
				currentLine = br.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private class SymptomsComparator implements Comparator<String> {

		@Override
		public int compare(String o1, String o2) {
			return o1.compareTo(o2);
		}
		
	}
	
	/**
	 * Using the already populated illnessBST, populateSymptomsBank() creates a new BST of all symptom options to be displayed to the user
	 */
	public void populateSymptomsBank() {
		for (Illness data: illnessBST) 
			for (String symptom: data.getSymptomBST()) {
				if (!symptomsBank.contains(symptom))
					symptomsBank.add(symptom);
			}
		symptomsBank.sort(new SymptomsComparator());
	}
	
	/**
	 * 
	 * @return BST of illnesses
	 */
	public BST<Illness> getIllnessBST() {
		return illnessBST;
	}

	/**
	 * 
	 * @return bufferedReader of the illness text file bank
	 */
	public BufferedReader getBr() {
		return br;
	}

	/**
	 * 
	 * @return symptoms bank containing all possible symptoms
	 */
	public LinkedList<String> getSymptomsBank() {
		return symptomsBank;
	}

}
