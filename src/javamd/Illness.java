package javamd;
import java.util.Scanner;
import java.io.*;
/**
 * Illness Class for JavaMD
 * Generic class for holding diverse illness information
 * @author metinaksu
 *
 */
public class Illness implements Comparable<Illness>{
	private String name;
	private String category;
	private int[] ageRange = new int[2];
	private BST<String> symptomBST = new BST<String>();
	private SexBias sex;
	private int weight;
	
	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	/**
	 * Illness constructor that takes in the name, affected age range, sex bias, category, and symptoms of any given illness
	 * @param pname
	 * @param pageRange
	 * @param psex
	 * @param pcategory
	 * @param psymptomBST
	 */
	public Illness(String pname, int[] pageRange, SexBias psex, String pcategory, BST<String> psymptomBST) {
		name = pname;
		ageRange = pageRange;
		sex = psex;
		category = pcategory;
		symptomBST = psymptomBST;
		weight = 0;
	}

	public Illness() {
		name = null;
		ageRange = null;
		sex = null;
		category = null;
		symptomBST = null;
		weight = 0;
	}
	
	/**
	 * As Illness implements comparable, the implementation of Illness.compareTo() is programmed to compare the names
	 * of the two Illness objects being compared in order to allow alphabetical sorting of Illnesses for JavaMDSetup.illnessBST
	 * @param o
	 * @return the lexicographic significance of the comparison (SEE String.compareTo())
	 */
	@Override
	public int compareTo(Illness o) {
		// TODO Auto-generated method stub
		return this.name.compareTo(o.getName());
	}
	
	/**
	 * 
	 * @return the name of the Illness
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @return the affected age range of the Illness
	 */
	public int[] getAgeRange() {
		return ageRange;
	}

	/**
	 * 
	 * @return the BST of symptoms of the Illness
	 */
	public BST<String> getSymptomBST() {
		return symptomBST;
	}

	/**
	 * 
	 * @return the sex bias of the Illness
	 */
	public SexBias getSex() {
		return sex;
	}

}
